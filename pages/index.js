// import Home from './home'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import Achievement from "@/components/achievement-component";
import Footer from "@/components/footer-component";
import NavBar from "@/components/navbar";
import Project from "@/components/project-compoenent";
import Services from "@/components/services-component";
import Specialskill from "@/components/specialskill-component";


const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <div className="px-10 bg-gray-800">
        <NavBar />
        <section className="flex w-full text-white ">
          <div className="flex items-center justify-center w-full ">
            <section>
              <div className="">
                <h1 className="text-xl">Hello!</h1>
                <h1 className="text-6xl font-bold"> I'm  Prajwol Basnet</h1>
              </div>
              <div className="">
                <p>Frontend Developer</p>
              </div>
            </section>
          </div>
          <div className="w-full ">
            <img src="/photo.png" />
          </div>
        </section >
        <Services />
        <Specialskill />
        <Achievement />
        <Project />
        {/* Footer */}
        <Footer />

      </div >
    </>
  )
}
