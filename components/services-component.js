import Card from "./card"

const services = [
    {
        id: 1,
        image: "/uiux.png",
        title: "UI/UX",
        features: [
            // "", "", "", "", "Mobile App Design"
            {
                id: 1,
                title: "Landing Page",
                classNames: "text-red-500"
            },
            {
                id: 2,
                title: "WireFraming",
                classNames: "text-blue-500"
            },
            {
                id: 3,
                title: "UserFlow",
                classNames: "text-yellow-500"
            },
            {
                id: 4,
                title: "Prototying",
                classNames: "text-white-500"
            },
            {
                id: 5,
                title: "Mobile App Design",
                classNames: "text-red-500"
            },

        ],


    },
    {
        id: 2,
        image: "/secondImg.png",
        title: "Development",
        features: [
            {
                id: 1,
                title: "HTML/CSS",
                classNames: "text-red-500"
            },
            {
                id: 2,
                title: "JavaScript",
                classNames: "text-blue-500"
            },
            {
                id: 3,
                title: "Animation",
                classNames: "text-yellow-500"
            },
            {
                id: 4,
                title: "WordPress",
                classNames: "text-white-500"
            },
            {
                id: 5,
                title: "React",
                classNames: "text-red-500"
            }
        ]

    },
    {
        id: 3,
        image: "/thirdImg.png",
        title: "Illustration",
        features: [
            {
                id: 1,
                title: "Character Design",
                classNames: "text-red-500"
            },
            {
                id: 2,
                title: "Icon Set",
                classNames: "text-blue-500"
            },
            {
                id: 3,
                title: "Illustration Set",
                classNames: "text-yellow-500"
            },
            {
                id: 4,
                title: "Illustration Guide",
                classNames: "text-white-500"
            },
            {
                id: 5,
                title: "Motion Graphic",
                classNames: "text-red-500"
            }
        ]

    }
]
const Services = () => {
    return <>
        <section className="w-full h-full text-white pt-11">
            <div className="">
                <span className="font-serif text-gray-300 text-md">
                    My Services
                </span>
                <h1 className="font-serif text-4xl ">
                    Services Provide For My Clients
                </h1>
            </div>
            <div className="flex justify-between">
                {services.map((service) => (<Card key={service.id}>
                    <div>
                        <div className="p-4 transition-all duration-300 bg-gray-900 group hover:rotate-1 lg:p-8">

                            <div className="flex items-center ">
                                <img className="aspect-[2/2] w-16" src="./uiux.png" />

                            </div>
                            <div className="my-4">
                                <h3 className="text-2xl font-medium text-gray-200">{service.title}</h3>
                                <div className="text-sm font-medium">
                                    <ul className="list-disc list-inside">
                                        {service.features.map((feature) => (
                                            <li className={`m-1 ml-0 ${feature.classNames}` }key={feature.id} >
                                                {feature.title}
                                            </li>
                                        ))

                                        }

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div >
                </Card >)
                )
                }

            </div>

        </section>
    </>
}

export default Services;
// specialSkill.map((skill) => (
                    // <div className="flex flex-col justify-center w-full gap-4 py-3 text-slate-400 ">
                    //     <div className="px-10 pt-10 pb-10 mb-2 bg-slate-700">
                    //         <section className="mb-1">
                    //             {skill.skill}
                    //         </section>
                    //         <div className="w-full h-2 bg-gray-300">
                    //             <div className={`bg-blue-700 w-[${skill.persent}] h-full relative`}>
                    //                 <div className="absolute right-0 w-5 h-5 -top-7">{skill.persent}</div>
                    //             </div>
                    //         </div>
                    //     </div>
                    // </div>
//                 ))