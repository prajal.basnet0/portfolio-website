
const achivements = [
    {
        id: 1,
        img: "/persion.png",
        number: "258+",
        title: "Happy Clients"
    },
    {
        id: 2,
        img: "/projectComplete.png",
        number: "590k",
        title: "Project Complete"
    },
    {
        id: 3,
        img: "/years.png",
        number: '28+',
        title: "Years of Exprerience"
    }
]

const Achievement = () => {
    return <>
        <div className="flex flex-row justify-between w-full py-16 text-white ">
            {
                achivements.map((achivement) => (
                    <div className="flex justify-start " key={achivement.id}>
                        <div className="flex justify-center w-16 h-16 p-4 border-2 rounded-lg border-amber-200">
                            <img src={`${achivement.img}`} alt="" />
                        </div>
                        <div className="flex flex-col mx-10 text-left">
                            <span className="mb-4 text-4xl ">{achivement.number}</span>
                            <span className="text-slate-400">{achivement.title}</span>
                        </div>
                    </div>
                ))
            }
        </div>
    </>
}

export default Achievement;
{/* <div className="flex justify-start ">
                <div className="flex justify-center w-16 h-16 p-4 border-2 rounded-lg border-amber-200">
                    <img src="./projectComplete.png" alt="" />
                </div>
                <div className="flex flex-col mx-10 text-left">
                    <span className="mb-4 text-4xl "> 590k</span>
                    <span className="text-slate-400">Project Complete</span>
                </div>
            </div>

            <div className="flex justify-start ">
                <div className="flex justify-center w-16 h-16 p-4 border-2 rounded-lg border-amber-200">
                    <img src="./years.png" alt="" />
                </div>
                <div className="flex flex-col mx-10 text-left">
                    <span className="mb-4 text-4xl "> 28+</span>
                    <span className="text-slate-400">Years of Experience</span>
                </div>
            </div> */}
