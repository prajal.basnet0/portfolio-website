
export default function Card({ children }) {
    return (
        <div className="pt-8 w-96 h-96 hover:shadow-lg">
            {children}
        </div>
    )
}
