const specialSkill = [
    {
        id: 1,
        skill: "Communications",
        persent: "90%"
    },
    {
        id: 2,
        skill: "Leadership",
        persent: "70%"
    },
    {
        id: 3,
        skill: "Teamwork",
        persent: "80%"
    },
    {
        id: 4,
        skill: "flexibility",
        persent: "75%"
    }
]

const Specialskill = () => {
    return <>
        <section className="flex w-full h-full text-white bg-gray-800 ">
            <div className="flex w-full my-10 ">
                <section>
                    <span className="font-serif text-gray-300 text-md">
                        Special Skills
                    </span>
                    <h1 className="font-serif text-6xl my-14">
                        My Special Skill Here.
                    </h1>
                    <a href="/Mern-Guide.pdf" color="transparent" download="mern-guide" target="_blank">
                        <button className="w-64 p-8 border-2 border-indigo-500 rounded-full my-14"  > Get Resume
                        </button>
                    </a>
                </section>
            </div>

            <div className="flex flex-col justify-center w-full gap-4 py-3 text-slate-400 ">
                {
                    specialSkill.map((skill) => (
                        <div className="px-10 pt-10 pb-10 mb-2 bg-slate-700" key={skill.id}>
                            <section className="mb-1">
                                {skill.skill}
                            </section>
                            <div className="w-full h-2 bg-gray-300">
                                <div className={`bg-blue-700 w-[${skill.persent}] h-full relative`}>
                                    <div className="absolute right-0 w-5 h-5 -top-7">{skill.persent}</div>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>


        </section>
    </>
}

export default Specialskill;
{/* <Carousel cols={2} rows={1} gap={100} loop>
    {
        projects.map((project) => (
            <Carousel.Item>
                <div className="h-full w-full">
                    <section className="w-full h-full ">
                        <div className="w-full h-[60%] px-14  bg-slate-700 rounded-md  pt-24">
                            <div className="w-full overflow-y-hidden h-[100%]">
                                <img src={`${project.img}`} className="object-left-top w-full " />
                            </div>
                        </div>
                        
                        <section className="flex flex-col justify-center mt-3 text-white">
                            <h1 className="text-2xl ">
                                {project.title}
                            </h1>
                            <span>{project.type}</span>
                        </section>
                    </section>
                </div>
            </Carousel.Item>
        ))
    }
</Carousel> */}


// const projects = [
//     {
//         id: 1,
//         img: "/img2.jpg",
//         title: "Givest- Non Profit PSD Template",
//         type: "Chairty/Fund Rising/Non Profit"
//     },
//     {
//         id: 2,
//         img: "/img2.jpg",
//         title: "Givest- Non Profit PSD Template",
//         type: "Chairty/Fund Rising/Non Profit"
//     }
// ]

