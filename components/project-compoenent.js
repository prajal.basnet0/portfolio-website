
import Carousel from "@christian-martins/react-grid-carousel";

const projects = [
    {
        id: 1,
        img: "/img2.jpg",
        title: "Givest- Non Profit PSD Template",
        type: "Chairty/Fund Rising/Non Profit"
    },
    {
        id: 2,
        img: "/img2.jpg",
        title: "Givest- Non Profit PSD Template",
        type: "Chairty/Fund Rising/Non Profit"
    },
    {
        id: 3,
        img: "/img2.jpg",
        title: "Givest- Non Profit PSD Template",
        type: "Chairty/Fund Rising/Non Profit"
    },
    {
        id: 4,
        img: "/img2.jpg",
        title: "Givest- Non Profit PSD Template",
        type: "Chairty/Fund Rising/Non Profit"
    }
]


const Project = () => {
    return <>
        <section className="w-full h-full text-white pt-11">
            <div className="h-screen text-stone-50">
                <div className="mb-7">
                    <span className="text-md text-slate-500 "> Awesome Portfolio</span>
                    <h1 className="my-3 font-serif text-4xl tracking-wide"> My Complete Projects</h1>
                </div>

                <div className="flex justify-between w-full h-full  ">
                    <Carousel cols={2} rows={1} gap={100} loop>
                        {
                            projects.map((project) => (
                                <Carousel.Item key={project.id}>
                                    <div className="h-full w-full">
                                        <section className="w-full h-full ">
                                            <div className="w-full h-[60%] px-14  bg-slate-700 rounded-md  pt-24">
                                                <div className="w-full overflow-y-hidden h-[100%]">
                                                    <img src={`${project.img}`} className="object-left-top w-full " />
                                                </div>
                                            </div>

                                            <section className="flex flex-col justify-center mt-3 text-white">
                                                <h1 className="text-2xl ">
                                                    {project.title}
                                                </h1>
                                                <span>{project.type}</span>
                                            </section>
                                        </section>
                                    </div>
                                </Carousel.Item>
                            ))
                        }
                    </Carousel>
                </div>


            </div>

        </section >
    </>
}

export default Project;